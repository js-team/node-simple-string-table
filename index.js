module.exports = function table(rows, padding = 2) {
	let spaces = rows.reduce(
		(spaces, row) =>
			row.reduce((spaces, value, index) => {
				value = "" + value;
				if (spaces[index] < value.length || spaces[index] == null) {
					spaces[index] = value.length;
				}
				return spaces;
			}, spaces),
		[]
	);
	return rows
		.map(row =>
			row
				.map(
					(value, index) =>
						value + " ".repeat(spaces[index] - value.length + padding)
				)
				.join("")
		)
		.join("\n");
};
