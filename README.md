# simple-string-table

It is created for the decoration of consoles, it allows to create a simple impression of a table based on an array, this script calculates the spaces of each column to justify the code.

```js
let table = require("simple-string-table");

let example = table([
	["GZIP", "BRO", "FILE"],
	["GZIP", "BRO123asdasdads", "FILE"],
	["GZIP", "BRO", "FILE"],
	["GZIP", "BRO12", "FILE"],
	["GZIP", "BRO3", "FILE"]
]);

console.log(example);
```

```bash
GZIP  BRO              FILE
GZIP  BRO123asdasdads  FILE
GZIP  BRO              FILE
GZIP  BRO12            FILE
GZIP  BRO3             FILE
```
